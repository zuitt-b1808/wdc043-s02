import java.util.*;

public class Main {
    public static void main(String[] args) {

        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};

        System.out.println("Fruits in stock: " + Arrays.toString(fruits));

        Scanner userInput = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of?");
        String searchTerm = userInput.nextLine();
        int result = Arrays.binarySearch(fruits, searchTerm);
        System.out.println("The index of " + searchTerm + " is: " + result);

        ArrayList<String> friends = new ArrayList<String>();
        //add entries to our ArrayList
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        //output contents of ArrayList friends in the console
        System.out.println("My friends are: " + friends);

        //using generics, create a HashMap that has elements with key of data type String and value of data type integer
        HashMap<String,Integer> inventory = new HashMap<String,Integer>();
        //add elements with matching key, value pairs
        inventory.put("toothbrush", 20);
        inventory.put("toothpaste", 15);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: ");
        System.out.println(inventory);








    }
}